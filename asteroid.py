# Final Version:
# http://www.codeskulptor.org/#user28_I5yxOIhy97mpyC8.py

# Developing Version:
# http://www.codeskulptor.org/#user27_e8oR88FGnL_21.py
# http://www.codeskulptor.org/#user27_UvAFcpvcu9_2.py

# Merry Christmas & Happy New Year 2014 

# The program was developed from a mini project of 'Interactive Programming in Python' course on Coursera
# Developer: nhatdx

import simplegui
import math
import random

# globals for user interface
WIDTH = 800
HEIGHT = 600
score = 0
lives = 3
time = 0
level = 1

started = False
spawn_started = False
hear = 0

MAXSPAWN = 4
max_spawn = MAXSPAWN

class ImageInfo:
    def __init__(self, center, size, radius = 0, lifespan = None, animated = False):
        self.center = center
        self.size = size
        self.radius = radius
        if lifespan:
            self.lifespan = lifespan
        else:
            self.lifespan = float('inf')
        self.animated = animated

    def get_center(self):
        return self.center

    def get_size(self):
        return self.size

    def get_radius(self):
        return self.radius

    def get_lifespan(self):
        return self.lifespan

    def get_animated(self):
        return self.animated

    
# art assets created by Kim Lathrop, may be freely re-used in non-commercial projects, please credit Kim    
debris_info = ImageInfo([320, 240], [640, 480])
debris_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/debris2_brown.png")

# nebula images
nebula_info = ImageInfo([400, 300], [800, 600])
nebula_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/nebula_blue.f2013.png")

# splash image
splash_info = ImageInfo([200, 150], [400, 300])
splash_image = simplegui.load_image("http://dl.dropboxusercontent.com/s/4q9wj41su0sefgp/splash2.png")

# ship image
ship_info = ImageInfo([45, 45], [90, 90], 35)
ship_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/double_ship.png")

# missile image - shot1.png, shot2.png, shot3.png
missile_info = ImageInfo([5,5], [10, 10], 3, 50)
missile_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/shot2.png")

# surprise image
sur_info = ImageInfo([29, 32], [59, 64], 25)
sur_image = simplegui.load_image("http://dl.dropboxusercontent.com/s/xct06h5cotvmx8n/surprise.png")

# LoL image
lol_info = ImageInfo([20, 23], [41, 47], 18)
lol_image = simplegui.load_image("http://dl.dropboxusercontent.com/s/s80fupb6t44uehl/lol.png")

# troll image
troll_info = ImageInfo([32, 32], [65, 64], 28)
troll_image = simplegui.load_image("http://dl.dropboxusercontent.com/s/ezec1ry7x8xw2l7/troll.png")

# cry image
cry_info = ImageInfo([33, 32], [66, 64], 28)
cry_image = simplegui.load_image("http://dl.dropboxusercontent.com/s/m20n7dy07xavsz4/cry.png")

# flirt image
fli_info = ImageInfo([33, 48], [67, 96], 30)
fli_image = simplegui.load_image("http://dl.dropboxusercontent.com/s/fj2t91gw2dldrv0/flirt.png")

# fucku image
fck_info = ImageInfo([33, 36], [67, 72], 29)
fck_image = simplegui.load_image("http://dl.dropboxusercontent.com/s/nh5w09nol0fv0ih/fuckyou.png")

# savage image
savage_info = ImageInfo([34, 32], [68, 64], 31)
savage_image = simplegui.load_image("http://dl.dropboxusercontent.com/s/p4ge8bux8unfv9a/savage.png")

# superman image
superman_info = ImageInfo([32, 32], [64, 64], 30)
superman_image = simplegui.load_image("http://dl.dropboxusercontent.com/s/xf5vpn33ehlhhgh/superman.png")

# x image size: 650 x 398
ghost_image = simplegui.load_image("http://dl.dropboxusercontent.com/s/qesnvwg28ospnu1/ghost-face.jpg")

# face_dict
palm_face = { sur_info: sur_image, lol_info: lol_image, troll_info: troll_image, cry_info: cry_image, fli_info: fli_image, fck_info: fck_image, savage_info: savage_image, superman_info: superman_image }

# asteroid images
asteroid_info = ImageInfo([45, 45], [90, 90], 40)
asteroid_sm1_info = ImageInfo([45, 45], [90, 90], 20)
asteroid_sm2_info = ImageInfo([45, 45], [90, 90], 10)
asteroid_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/asteroid_blue.png")

explosion_info = ImageInfo([64, 64], [128, 128], 17, 24, True)
explosion_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/explosion_alpha.png")

soundtrack = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/soundtrack.mp3")
soundtrack.set_volume(.015)
missile_sound = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/missile.mp3")
missile_sound.set_volume(.015)
ship_thrust_sound = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/thrust.mp3")
ship_thrust_sound.set_volume(.015)
explosion_sound = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/explosion.mp3")
explosion_sound.set_volume(.015)

# pass sound
pass_sound = simplegui.load_sound("http://dl.dropboxusercontent.com/s/m9at8fvxz7pwjoh/pass_sound.mp3")
pass_diec_sound = simplegui.load_sound("http://dl.dropboxusercontent.com/s/lbq3w765bae6ah2/pass_diec_sound.mp3")

# x sound
scream_sound = simplegui.load_sound("http://dl.dropboxusercontent.com/s/8qjon8ng2vjxkmo/Horror_Scream.mp3")
scream_sound.set_volume(1)

# helper functions to handle transformations
def angle_to_vector(ang):
    return [math.cos(ang), math.sin(ang)]

def dist(p, q):
    return math.sqrt((p[0] - q[0]) ** 2 + (p[1] - q[1]) ** 2)


# Ship class
class Ship:
    def __init__(self, pos, vel, angle, image, info):
        self.pos = [pos[0], pos[1]]
        self.vel = [vel[0], vel[1]]
        self.thrust = False
        self.angle = angle
        self.angle_vel = 0
        self.image = image
        self.image_center = info.get_center()
        self.image_size = info.get_size()
        self.radius = info.get_radius()
        
    def draw(self,canvas):
        if self.thrust:
            canvas.draw_image(self.image, [self.image_center[0] + self.image_size[0], self.image_center[1]] , self.image_size,
                              self.pos, self.image_size, self.angle)
        else:
            canvas.draw_image(self.image, self.image_center, self.image_size,
                              self.pos, self.image_size, self.angle)

    def update(self):
        
        # update angle
        self.angle += self.angle_vel
        
        # update position
        self.pos[0] = (self.pos[0] + self.vel[0]) % WIDTH
        self.pos[1] = (self.pos[1] + self.vel[1]) % HEIGHT

        # update velocity
        if self.thrust:
            acc = angle_to_vector(self.angle)
            self.vel[0] += acc[0] * .1
            self.vel[1] += acc[1] * .1
            
        self.vel[0] *= .99
        self.vel[1] *= .99

    def set_thrust(self, on):
        self.thrust = on
        if on:
            ship_thrust_sound.rewind()
            ship_thrust_sound.play()
        else:
            ship_thrust_sound.pause()
       
    def increment_angle_vel(self):
        self.angle_vel += .07
        
    def decrement_angle_vel(self):
        self.angle_vel -= .07
        
    def reset_ang_vel(self):
        self.angle_vel = 0
        
    def shoot(self):
        global missile_grp
        forward = angle_to_vector(self.angle)
        missile_pos = [self.pos[0] + self.radius * forward[0], self.pos[1] + self.radius * forward[1]]
        missile_vel = [self.vel[0] + 6 * forward[0], self.vel[1] + 6 * forward[1]]
        a_missile = Sprite(missile_pos, missile_vel, self.angle, 0, missile_image, missile_info, 1, missile_sound)
        
        missile_grp.add(a_missile)
    
    
# Sprite class
class Sprite:
    def __init__(self, pos, vel, ang, ang_vel, image, info, divide = 1, sound = None):
        self.pos = [pos[0],pos[1]]
        self.vel = [vel[0],vel[1]]
        self.angle = ang
        self.angle_vel = ang_vel
        self.image = image
        self.image_center = info.get_center()
        self.image_size = info.get_size()
        self.divide = divide
        self.radius = info.get_radius() / self.divide
        self.lifespan = info.get_lifespan()
        self.animated = info.get_animated()
        self.show_size = [self.image_size[0] / self.divide, self.image_size[1] / self.divide]
        self.age = 0
        if sound:
            sound.rewind()
            sound.play()
   
    def draw(self, canvas):
        if self.animated:
            animated_x = (self.age % self.lifespan) * self.image_size[0]
            canvas.draw_image(self.image, [ self.image_center[0]+animated_x, self.image_center[1] ], self.image_size, self.pos, self.show_size)
        else:
            canvas.draw_image(self.image, self.image_center, self.image_size,
                          self.pos, self.show_size, self.angle)

    def update(self):
        # update angle
        self.angle += self.angle_vel
        
        # update position
        self.pos[0] = (self.pos[0] + self.vel[0]) % WIDTH
        self.pos[1] = (self.pos[1] + self.vel[1]) % HEIGHT
        
        self.age += 1
        if self.lifespan != None:
            return self.age > self.lifespan
        else:
            return False
                
    def collide(self, other_obj):
        distance = dist(self.pos, other_obj.pos)
        return distance <= self.radius + other_obj.radius
    
    def get_divide(self):
        return self.divide
        
          
# key handlers to control ship   
def keydown(key):
    if not started:
        my_ship.reset_ang_vel()
        return
    
    if key == simplegui.KEY_MAP['left']:
        my_ship.decrement_angle_vel()
    elif key == simplegui.KEY_MAP['right']:
        my_ship.increment_angle_vel()
    elif key == simplegui.KEY_MAP['up']:
        my_ship.set_thrust(True)
    elif key == simplegui.KEY_MAP['space']:
        my_ship.shoot()
        
def keyup(key):
    if not started:
        my_ship.reset_ang_vel()
        return
    
    if key == simplegui.KEY_MAP['left']:
        my_ship.increment_angle_vel()
    elif key == simplegui.KEY_MAP['right']:
        my_ship.decrement_angle_vel()
    if key == simplegui.KEY_MAP['up']:
        my_ship.set_thrust(False)
        
def process_sprite_group(canvas, grp):
    rmv_set = set()
    
    for item in grp:
        item.draw(canvas)
        if item.update():
            rmv_set.add(item)

    grp.difference_update(rmv_set)            
                
def group_collide(grp_obj, an_obj, allow_div = True):
    for obj in list(grp_obj):
        if obj.collide(an_obj):
            grp_obj.discard(obj)

            a_new_explosion = Sprite(obj.pos, [0, 0], 0, 0, explosion_image, explosion_info, obj.divide, explosion_sound)
            explosion_grp.add(a_new_explosion)                  
            
            RATE = 1.3
            if obj.divide < RATE**2 and allow_div:            
                vel_magnitude = math.sqrt( obj.vel[0]**2 + obj.vel[1]**2 )
                
                new_angle = math.acos(obj.vel[0] / vel_magnitude)
                if obj.vel[1] > 0:
                    new_angle = math.radians(360) - new_angle
                                
                new_angle1 = new_angle + math.radians(60)
                
                new_angle2 = new_angle - math.radians(60)
                
                vel_magnitude *= 2
                new_vel1 = [ vel_magnitude * math.cos(new_angle1), -vel_magnitude * math.sin(new_angle1) ]
                new_vel2 = [ vel_magnitude * math.cos(new_angle2), -vel_magnitude * math.sin(new_angle2) ]

                show_info = random.choice( palm_face.keys() )
                show_image = palm_face[ show_info ]                
                a_new_rock1 = Sprite(obj.pos, new_vel1, 0, obj.angle_vel, show_image, show_info, obj.divide * RATE)

                show_info = random.choice( palm_face.keys() )
                show_image = palm_face[ show_info ]                
                a_new_rock2 = Sprite(obj.pos, new_vel2, 0, obj.angle_vel, show_image, show_info, obj.divide * RATE)

                rock_grp.add(a_new_rock1)
                rock_grp.add(a_new_rock2)
                
            return True
    return False            

def group_group_collide(grp_obj1, grp_obj2):
    num_collide = 0
    for obj in list(grp_obj1):
        if group_collide(grp_obj2, obj):
            grp_obj1.discard(obj)
            num_collide +=1
    return num_collide

def reset():        
    global rock_grp, missile_grp, explosion_grp, started
    started = False        
    
    rock_grp = set()
    missile_grp = set()
    explosion_grp = set()
    
    my_ship.pos = [WIDTH / 2, HEIGHT / 2]
    my_ship.vel = [0, 0]
    my_ship.ang_vel = 0
    my_ship.angle = 0
    my_ship.set_thrust(False)                        
    
def ghost_appear(canvas):
    global time
    if time % 7 < 5:
        canvas.draw_image( ghost_image, (325, 199), (650, 398), (WIDTH/2, HEIGHT/2), (WIDTH, HEIGHT) )
    else:
        canvas.draw_circle( (WIDTH/2, HEIGHT/2), WIDTH + HEIGHT, 1, "Black", "Black" )
    scream_sound.play()
    
def tell_pass():            
    global hear
    hear += 1    
    if hear < 3:
        pass_sound.set_volume(.03)    
        pass_sound.play()
    else:
        pass_diec_sound.set_volume(.03)
        pass_diec_sound.play()    

def check_pass(text_input):
    global started, score, level, lives, spawn_started, time, max_spawn
    if text_input == str(int(3.14 * 481.6)) and not started:
        score = 0
        level = 1
        lives = 3
        started = True
        spawn_started = False
        time = 0
        max_spawn = MAXSPAWN
    
def draw(canvas):
    global time, started, lives, score, level, max_spawn, spawn_started
        
    # animiate background
    time += 1
    wtime = (time / 4) % WIDTH
    center = debris_info.get_center()
    size = debris_info.get_size()
    canvas.draw_image(nebula_image, nebula_info.get_center(), nebula_info.get_size(), [WIDTH / 2, HEIGHT / 2], [WIDTH, HEIGHT])
    canvas.draw_image(debris_image, center, size, (wtime - WIDTH / 2, HEIGHT / 2), (WIDTH, HEIGHT))
    canvas.draw_image(debris_image, center, size, (wtime + WIDTH / 2, HEIGHT / 2), (WIDTH, HEIGHT))

    # draw UI
    canvas.draw_text("Lives", [50, 50], 30, "LightGreen")
    canvas.draw_text("Score", [580, 50], 30, "White")
    canvas.draw_text("Level", [680, 50], 30, "Yellow")
    canvas.draw_text(str(lives), [50, 80], 23, "LightGreen")
    canvas.draw_text(str(score), [580, 80], 23, "White")
    canvas.draw_text(str(level), [680, 80], 23, "Yellow")

    my_ship.draw(canvas)
            
    # draw splash screen if not started        
    if not started:
        if time < 240:
            canvas.draw_text("Cho 3s de chuong trinh load du lieu", [230, 130], 23, "Red")    
        canvas.draw_image(splash_image, splash_info.get_center(), splash_info.get_size(), [WIDTH / 2, HEIGHT / 2], splash_info.get_size())            
        
    # draw and update a group of rocks
    process_sprite_group(canvas, rock_grp)
    process_sprite_group(canvas, missile_grp)
    process_sprite_group(canvas, explosion_grp)
            
    # collide between rocks and ship
    if group_collide(rock_grp, my_ship, False):
        lives -= 1            
                         
    # collide between rocks and missiles
    score += group_group_collide(missile_grp, rock_grp)    
    if not max_spawn and score >= ( level * (level+1) // 2 ) * MAXSPAWN * 5:
        level += 1
        max_spawn = level * MAXSPAWN

    # check rock_grp
    if rock_grp == set([]) and spawn_started:
        max_spawn = 1
        
    if started:
        my_ship.update()    
                
        if (score > 45 or lives <= 0) and time > 1200: #20s
            ghost_appear(canvas)            
        elif lives == 0:
            reset()        
        
# spawn rocks        
def rock_spawner():    
    global rock_grp, max_spawn, palm_face, spawn_started
    
    if not started or not max_spawn:
        return                
    
    rock_pos = [random.randrange(0, WIDTH), random.randrange(0, HEIGHT)]
    rock_vel = [random.random() * .6 - .2, random.random() * .6 - .2]
    rock_avel = random.random() * .2 - .1

    show_info = random.choice( palm_face.keys() )
    show_image = palm_face[ show_info ]    
    a_new_rock = Sprite(rock_pos, rock_vel, 0, rock_avel, show_image, show_info, 1)
    
    if dist(a_new_rock.pos, my_ship.pos) >= 1.8 * (a_new_rock.radius + my_ship.radius):
        max_spawn -= 1
        rock_grp.add(a_new_rock)
        spawn_started = True
            
# initialize stuff
frame = simplegui.create_frame("Game Bua - Developed by Nhatdx", WIDTH, HEIGHT)

# initialize ship and two sprites
my_ship = Ship([WIDTH / 2, HEIGHT / 2], [0, 0], 0, ship_image, ship_info)

reset()

# register handlers
frame.set_keyup_handler(keyup)
frame.set_keydown_handler(keydown)
frame.set_draw_handler(draw)

label00 = frame.add_label('')
label01 = frame.add_label('')
label02 = frame.add_label('')
label03 = frame.add_label('')
label04 = frame.add_label('')
label05 = frame.add_label('')

button1 = frame.add_button( 'Click de nghe mat khau', tell_pass )
label06 = frame.add_label('')

input1 = frame.add_input( 'Nhap mat khau (Nhap xong Enter):', check_pass, 100 )

label0 = frame.add_label('')
label1 = frame.add_label('Dung cac phim TRAI, PHAI, TREN de di chuyen, phim SPACE de ban')

timer = simplegui.create_timer(1500.0, rock_spawner)

# get things rolling
timer.start()
frame.start()